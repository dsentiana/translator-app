$(document).ready(function () {
  $("#translate").on("click", function () {
    var lang1 = $("#lang1").val();
    var lang2 = $("#lang2").val();
    var text = $("#text").val();
    var res = $("#res").val();

    $.ajax({
      url: "process.php",
      type: "post",
      data: { lang1: lang1, lang2: lang2, text: text },
      success: function (status) {
        console.log(status);
        res = $("#res").val(status);
      },
    });
  });
});
