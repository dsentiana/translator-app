To run this translator app, it takes software that is already installed, 
namely XAMPP (download from here https://www.apachefriends.org/download.html )
 
1. Run the XAMPP control, and start Apache
2. Extract this archive into C:\xampp\htdocs
3. Run a browser and type or copy this "localhost/translate" with no quotation mark


API source      : https://github.com/Stichoza/google-translate-php
CSS Framework   : Bootstrap
Source language : HTML, CSS, JS, PHP
Targeet language: HTML, CSS, JS, PHP