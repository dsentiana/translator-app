<?php

require_once 'vendor/autoload.php';

use Stichoza\GoogleTranslate\GoogleTranslate;

$lang1 = $_POST['lang1'];
$lang2 = $_POST['lang2'];
$text = $_POST['text'];

if ($lang1 == 'AutoDetect') {
    $tr = new GoogleTranslate($lang2);

    $text = $tr->translate($text);

    $lang1 = $tr->getLastDetectedSource(); 

    echo GoogleTranslate::trans($text, $lang2, $lang1);

}else{
    echo GoogleTranslate::trans($text, $lang2, $lang1);
}


